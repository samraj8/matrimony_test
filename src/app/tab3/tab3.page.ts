import { Component, OnInit } from '@angular/core';
import { log } from 'console';
import { from } from 'rxjs';
import { ServiceService } from './../service.service';  
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
cards:any;

slideOpts = {
  initialSlide: 0,
  speed: 400,
  slidesPerView: 1,
  loop:true,
  spaceBetween: 20,
  coverflowEffect: {
    // rotate: 50,
    stretch: 0,
    // depth: 100,
    modifier: 1,
    slideShadows: true,
  },
  isBeginningSlide: true,
  isEndSlide: true,
};
  constructor(private service:ServiceService) { 
   }
ngOnInit(){ 
this.cards = this.service.passData; 
}

  
}
