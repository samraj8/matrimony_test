import { Injectable } from '@angular/core';
import { Subject,Subscription } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private channels: { [key: string]: Subject<any>; } = {};

  constructor() { }
  private fooSubject = new Subject<any>();
  public passData:any;
  publishSomeData(data: any) {
      this.fooSubject.next(data); 
  }

  getObservable(): Subject<any> {
      return this.fooSubject;
  }

}
