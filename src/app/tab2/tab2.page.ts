import { Component, Injectable, ViewChild } from '@angular/core';
import { Router } from '@angular/router'; 
import { ServiceService } from './../service.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page { 
  NextSlide: any;
  cards:any=[];
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    slidesPerView: 1.5,
    loop:true,
    spaceBetween: 20,
    coverflowEffect: {
      // rotate: 50,
      stretch: 0,
      // depth: 100,
      modifier: 1,
      slideShadows: true,
    },
    isBeginningSlide: true,
    isEndSlide: true,
  };
 
  constructor(private route:Router,private service:ServiceService)  {
    this.loadImage();
  }
  
  loadImage(){
    this.cards = [
      {
        name:"Name One",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/people",
      },
      {
        name:"Name Two",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/arch",      
      },
      {
        name:"Name Three",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/nature",
      },
      {
        name:"Name Four",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/tech",       
      },
      {
        name:"Name Five",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/arch",       
      },
    ];
  }
 

  openTab3(data){  
    this.service.passData = data; 
    if(this.service.passData)  this.route.navigateByUrl('tab3');
  }

  movePage(){ 
    this.route.navigateByUrl('tabs/tab2');
  }
 
}

