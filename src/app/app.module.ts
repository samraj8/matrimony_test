import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG,HammerModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HammerDirective } from './hammer.directive';
import { ServiceService } from './service.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,BrowserAnimationsModule,HammerModule],
  providers: [
    StatusBar,
    SplashScreen,
    ServiceService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide:HAMMER_GESTURE_CONFIG,useClass:HammerDirective}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
