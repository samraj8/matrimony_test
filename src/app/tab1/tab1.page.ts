import { Component, Input, ViewChildren, QueryList, ElementRef, EventEmitter, Output, Renderer2 } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations'; 
import { Indicator, IndicatorAnimations } from '../indicators';
import 'hammerjs';

import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  animations:IndicatorAnimations
})
export class Tab1Page {
  @Input('cards') cards: Array<{
    name:string,
    img: string,
    title: string,
    description: string,
    detail:string
  }>;

  @ViewChildren('tinderCard') tinderCards: QueryList<ElementRef>;
  tinderCardsArray: Array<ElementRef>;

  @Output() choiceMade = new EventEmitter();

  moveOutWidth: number;
  shiftRequired: boolean;
  transitionInProgress: boolean;
  heartVisible: boolean;
  crossVisible: boolean;



  eventText = '';
  indicators; 

  constructor(private renderer: Renderer2,public toastController: ToastController,private route:Router) {
    this.indicators = new Indicator();
    this.loadImage();
  }

  loadImage(){
    this.cards = [
      {
        name:"Name One",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/people",
      },
      {
        name:"Name Two",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/arch",      
      },
      {
        name:"Name Three",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/nature",
      },
      {
        name:"Name Four",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/tech",       
      },
      {
        name:"Name Five",
        detail: "27 Yrs,5 ft in,MBBS,Doctor,Poosam",
        title: "Hindu - Kayasta, Chennai",
        description: "Tamil Nadu.",
        img: "https://placeimg.com/300/300/arch",       
      },
    ];
  }

  swipeEvent(ev) {
    ev.preventDefault();
    switch (ev.direction) {
      case 4:
        this.renderer.setStyle(this.tinderCardsArray[0].nativeElement, 'transform', 'translate(' + this.moveOutWidth + 'px, -100px) rotate(-30deg)')
        this.presentToast('Interested');
        this.emitChoice(true, this.cards[0]);
        this.shiftRequired = true;
      this.transitionInProgress = true;
        break;
      case 2:
        this.renderer.setStyle(this.tinderCardsArray[0].nativeElement, 'transform', 'translate(-' + this.moveOutWidth + 'px, -100px) rotate(30deg)')
        this.presentToast('Not Interested');
        this.emitChoice(false, this.cards[0]);
        this.shiftRequired = true;
      this.transitionInProgress = true;
        break;
      case 8:
        this.renderer.setStyle(this.tinderCardsArray[0].nativeElement, 'transform', 'translate(' + 0 + 'px, -100px) rotate(360deg)')
        this.presentToast('Shortlisted');
      this.emitChoice(false, this.cards[0]); 
      this.shiftRequired = true;
      this.transitionInProgress = true;
      default:      
        break;
       
    }
  } 

  userClickedButton(event, heart) {
    console.log(heart);    
    event.preventDefault();
    if (!this.cards.length) return false;
    if (heart == 'right') {
      this.renderer.setStyle(this.tinderCardsArray[0].nativeElement, 'transform', 'translate(' + this.moveOutWidth + 'px, -100px) rotate(-30deg)')
      this.presentToast('Interested');
      this.emitChoice(heart, this.cards[0]);
    } else if(heart == 'left') {
      this.renderer.setStyle(this.tinderCardsArray[0].nativeElement, 'transform', 'translate(-' + this.moveOutWidth + 'px, -100px) rotate(30deg)')
      
      this.presentToast('Not Interested');
      this.emitChoice(heart, this.cards[0]);
    } else {
      this.renderer.setStyle(this.tinderCardsArray[0].nativeElement, 'transform', 'translate(' + 0 + 'px, -100px) rotate(360deg)')
      
      this.presentToast('Shortlisted');
      this.emitChoice(heart, this.cards[0]); 
    };
    this.shiftRequired = true;
    this.transitionInProgress = true;
  };

  async presentToast(message?) {
    const toast = await this.toastController.create({
      message: message,
      duration: 500
    });
    toast.present();

    if(this.cards.length == 1){
      this.loadImage();
    }
  }

  handleShift() {
    this.transitionInProgress = false; 
    if (this.shiftRequired) {
      this.shiftRequired = false;
      this.cards.shift();
    };
  };

  emitChoice(heart, card) {
    this.choiceMade.emit({
      choice: heart,
      payload: card
    })
  };

  ngAfterViewInit() {
    this.moveOutWidth = document.documentElement.clientWidth * 1.5;
    this.tinderCardsArray = this.tinderCards.toArray();
    this.tinderCards.changes.subscribe(()=>{
      this.tinderCardsArray = this.tinderCards.toArray();
    })
  };

  movePage(){ 
    this.route.navigateByUrl('tabs/tab1');
  }
}
